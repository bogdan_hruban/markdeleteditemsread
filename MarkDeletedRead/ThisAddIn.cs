﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

namespace MarkDeletedItemsRead
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            Outlook.MAPIFolder deletedFolder = this.Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderDeletedItems);
            deletedFolder.Items.ItemAdd += new Outlook.ItemsEvents_ItemAddEventHandler(DeletedItems_ItemAdd);
        }

        void DeletedItems_ItemAdd(object Item)
        {
            if(Item is Outlook.MailItem)
            {
                (Item as Outlook.MailItem).UnRead = false;
            }
            else if (Item is Outlook.AppointmentItem)
            {
                (Item as Outlook.AppointmentItem).UnRead = false;
            }
            else if (Item is Outlook.MeetingItem)
            {
                (Item as Outlook.MeetingItem).UnRead = false;
            }
            else if (Item is Outlook.ContactItem)
            {
                (Item as Outlook.ContactItem).UnRead = false;
            }
            else if (Item is Outlook.JournalItem)
            {
                (Item as Outlook.JournalItem).UnRead = false;
            }
            else if (Item is Outlook.TaskItem)
            {
                (Item as Outlook.TaskItem).UnRead = false;
            }
            else if (Item is Outlook.TaskRequestItem)
            {
                (Item as Outlook.TaskRequestItem).UnRead = false;
            }
            else if (Item is Outlook.PostItem)
            {
                (Item as Outlook.PostItem).UnRead = false;
            }
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
